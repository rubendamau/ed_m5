package m09Uf3Act12;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Client3Objecte {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		InetAddress desti = InetAddress.getLocalHost();
		int port = 12345;
		
		Persona persona = new Persona("personaUDP", 25);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(baos);
		out.writeObject(persona);
		out.close();
		
		byte[] ObjectBytes = baos.toByteArray();
		
		DatagramPacket enviament = new DatagramPacket
				   (ObjectBytes, ObjectBytes.length, desti, port);
		DatagramSocket socket = new DatagramSocket(34567);
		
		System.out.println("Envian desde el client: "+persona.getNom()+", "+persona.getEdat());
		socket.send(enviament);
		
		byte[] bufer = new byte[1024];				
		DatagramPacket recepcio = new DatagramPacket(bufer, bufer.length);		
		socket.receive(recepcio);
		
		ByteArrayInputStream bais = new ByteArrayInputStream(bufer);
		ObjectInputStream in = new ObjectInputStream(bais);
		
		persona= (Persona) in.readObject();
		in.close();
		
		System.out.println("Reben al client: "+persona.getNom()+", "+persona.getEdat());
		
		socket.close();
	}

}
