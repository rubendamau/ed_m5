package m09Uf3Act12;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client2Objecte {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		Socket client = new Socket("localhost", 6000);
		
		Empleat empleat = new Empleat("empleat1", "111111j", 1000);
		
		System.out.println("Envian desde el client: "+empleat.getNom()+", "+empleat.getDni()+", "+empleat.getSalari());
		ObjectOutputStream outObject = new ObjectOutputStream(client.getOutputStream());
		outObject.writeObject(empleat);
		
		ObjectInputStream inObject = new ObjectInputStream(client.getInputStream());
		empleat = (Empleat) inObject.readObject();
		
		System.out.println("Reben al client: "+empleat.getNom()+", "+empleat.getDni()+", "+empleat.getSalari());
		
	}

}
