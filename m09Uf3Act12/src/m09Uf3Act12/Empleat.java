package m09Uf3Act12;

import java.io.Serializable;

public class Empleat implements Serializable {
	String nom;
	String dni;
	int salari;
	
	public Empleat(){
		
	}
	
	public Empleat(String nom, String dni,int salari) {
		// TODO Auto-generated constructor stub
		this.nom = nom;
		this.dni = dni;
		this.salari = salari;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getSalari() {
		return salari;
	}

	public void setSalari(int salari) {
		this.salari = salari;
	}
	
}
