package m09Uf3Act12;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor2Objecte {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		int numeroPort = 6000;
		ServerSocket servidor = new ServerSocket(numeroPort);
		System.out.println("Esperant al client...");
		Socket client = servidor.accept();
		
		ObjectInputStream inObject = new ObjectInputStream(client.getInputStream());
		Empleat empleat = (Empleat) inObject.readObject();
		
		System.out.println("Reben al servidor: "+empleat.getNom()+", "+empleat.getDni()+", "+empleat.getSalari());
		
		empleat.setNom("empleatMod2");
		empleat.setDni("222222k");
		empleat.setSalari(2000);
		
		System.out.println("Envian desde el servidor: "+empleat.getNom()+", "+empleat.getDni()+", "+empleat.getSalari());
		
		ObjectOutputStream outObject = new ObjectOutputStream(client.getOutputStream());
		outObject.writeObject(empleat);
		
		outObject.close();
		inObject.close();
		client.close();
		servidor.close();				
	}

}
