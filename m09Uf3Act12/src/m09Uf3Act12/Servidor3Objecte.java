package m09Uf3Act12;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Servidor3Objecte {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		DatagramSocket socket = new DatagramSocket(12345);
		System.out.println("\nEsperant Datagrama");
		
		byte[] bufer = new byte[1024];				
		DatagramPacket recepcio = new DatagramPacket(bufer, bufer.length);		
		socket.receive(recepcio);
		
		ByteArrayInputStream bais = new ByteArrayInputStream(bufer);
		ObjectInputStream in = new ObjectInputStream(bais);
		
		Persona persona= (Persona) in.readObject();
		System.out.println("Reben al servidor: "+persona.getNom()+", "+persona.getEdat());
		
		persona.setNom("personaModUDP");
		persona.setEdat(20);
		
		InetAddress desti = InetAddress.getLocalHost();
		int port = 34567;
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(baos);
		out.writeObject(persona);
		out.close();
		
		byte[] ObjectBytes = baos.toByteArray();
		
		DatagramPacket enviament = new DatagramPacket
				   (ObjectBytes, ObjectBytes.length, desti, port);
		System.out.println("Envian desde el client: "+persona.getNom()+", "+persona.getEdat());
		socket.send(enviament);
	}

}
