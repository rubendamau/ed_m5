package m09Uf3Act12;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor1Objecte {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		int numeroPort = 6000;
		ServerSocket servidor = new ServerSocket(numeroPort);
		System.out.println("Esperant al client...");
		Socket client = servidor.accept();
		
		ObjectOutputStream outObjecte = new ObjectOutputStream(client.getOutputStream());
		Persona per = new Persona("Adria",31);
		
		outObjecte.writeObject(per);
		System.out.println("Enviat: "+per.getNom()+"#"+per.getEdat());
		
		ObjectInputStream inObjecte = new ObjectInputStream(client.getInputStream());
		Persona per2 = (Persona) inObjecte.readObject();
		System.out.println("Rebut: "+per2.getNom()+"#"+per.getEdat());
		
		outObjecte.close();
		inObjecte.close();
		client.close();
		servidor.close();		
	}

}
