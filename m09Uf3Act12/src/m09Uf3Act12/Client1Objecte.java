package m09Uf3Act12;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client1Objecte {

	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		Socket client = new Socket("localhost", 6000);
		
		ObjectInputStream inObjecte = new ObjectInputStream(client.getInputStream());
		Persona per = (Persona) inObjecte.readObject();
		
		ObjectOutputStream outObjecte = new ObjectOutputStream(client.getOutputStream());
		outObjecte.writeObject(per);
		
		client.close();
	}

}
